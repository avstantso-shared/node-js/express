import { BaseError, Details } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';
import { ModelErrorKind } from '@avstantso/node-or-browser-js--model-core';
import { ModelError } from '@avstantso/node-js--modeler';

export type ErrorCallback = (error: Error) => Promise<any>;

export class ApiError extends BaseError {
  status: number;
  details: Details.Union;

  constructor(status: number, message: string, internal?: Error) {
    super(message, internal);
    this.status = status;

    Object.setPrototypeOf(this, ApiError.prototype);
  }

  static is(error: unknown): error is ApiError {
    return JS.is.error(this, error);
  }

  static badRequest(message: string, internal?: Error): ApiError {
    return new ApiError(400, message, internal);
  }

  static forbidden(message: string, internal?: Error): ApiError {
    return new ApiError(403, message, internal);
  }

  static notFound(message: string, internal?: Error): ApiError {
    return new ApiError(404, message, internal);
  }

  static conflict(message: string, internal?: Error): ApiError {
    return new ApiError(409, message, internal);
  }

  static tooManyRequests(message: string, internal?: Error): ApiError {
    return new ApiError(429, message, internal);
  }

  static internal(message: string, internal?: Error): ApiError {
    return new ApiError(500, message, internal);
  }

  static handelModelErrors(): ErrorCallback {
    return (e) => {
      // @ts-ignore fix CI TS2589: Type instantiation is excessively deep and possibly infinite
      if (ModelError.is(e) ? !!e.kind : true) throw e;

      throw ApiError.badRequest(e.message, e);
    };
  }

  static catcher<T>(promise: Promise<T>): Promise<T> {
    return promise.catch(ApiError.handelModelErrors());
  }

  protected toLogDetails(): object {
    return {
      ...super.toLogDetails(),
      status: this.status,
      ...(this.details ? { details: this.details } : {}),
    };
  }
}
