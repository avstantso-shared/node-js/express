import { MESSAGES } from '@avstantso/node-or-browser-js--model-core';

function paginationNumMustBeEqualsOrGreaterThan0(num: number) {
  return `pagination.num must be equals or greater than 0 but it's ${num}`;
}

function paginationSizeMustBeGreaterThan0(size: number) {
  return `pagination.size must be equals or greater than 0 but it's ${size}`;
}
export default {
  ...MESSAGES,
  paginationNumMustBeEqualsOrGreaterThan0,
  paginationSizeMustBeGreaterThan0,
};
