import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type * as Modeler from '@avstantso/node-js--modeler';

import type { Controller as Types } from './types';
import _Readable from './readable';
import _Writable from './writable';
import _ReadableWritable from './readable-writable';

export { Safe, validateData, ModelerQuery } from './utils';

export type GetModeler<
  TModel extends Model.Structure,
  TModeler extends Modeler.Union.Any<TModel>
> = Types.GetModeler<TModel, TModeler>;

export type Options<
  TModel extends Model.Structure,
  TModeler extends Modeler.Union.Any<TModel>
> = Types.Options<TModel, TModeler>;

export namespace Readable {
  export type Options<
    TModel extends Model.Structure,
    TModeler extends Modeler.Readable<TModel>
  > = Types.Readable.Options<TModel, TModeler>;
}

export type Readable = Types.Readable;
export const Readable = _Readable;

export namespace Writable {
  export namespace Extender {
    export namespace Context {
      export type Insert<
        TModel extends Model.Structure,
        TModeler extends Modeler.Writable<TModel>
      > = Types.Writable.Extender.Context.Insert<TModel, TModeler>;

      export type Update<
        TModel extends Model.Structure,
        TModeler extends Modeler.Writable<TModel>
      > = Types.Writable.Extender.Context.Update<TModel, TModeler>;

      export type Delete<
        TModel extends Model.Structure,
        TModeler extends Modeler.Writable<TModel>
      > = Types.Writable.Extender.Context.Delete<TModel, TModeler>;
    }

    export type Context<
      TModel extends Model.Structure,
      TModeler extends Modeler.Writable<TModel>
    > = Types.Writable.Extender.Context<TModel, TModeler>;

    export type Insert<
      TModel extends Model.Structure,
      TModeler extends Modeler.Writable<TModel>
    > = Types.Writable.Extender.Insert<TModel, TModeler>;

    export type Update<
      TModel extends Model.Structure,
      TModeler extends Modeler.Writable<TModel>
    > = Types.Writable.Extender.Update<TModel, TModeler>;

    export type Delete<
      TModel extends Model.Structure,
      TModeler extends Modeler.Writable<TModel>
    > = Types.Writable.Extender.Delete<TModel, TModeler>;
  }

  export type Extender<
    TModel extends Model.Structure,
    TModeler extends Modeler.Writable<TModel>
  > = Types.Writable.Extender<TModel, TModeler>;

  export type IOptions<
    TModel extends Model.Structure,
    TModeler extends Modeler.Writable<TModel>
  > = Types.Writable.Options<TModel, TModeler>;
}

export type Writable = Types.Writable;
export const Writable = _Writable;

export namespace ReadableWritable {
  export type Options<
    TModel extends Model.Structure,
    TModeler extends Modeler.ReadableWritable<TModel>
  > = Types.ReadableWritable.Options<TModel, TModeler>;
}

export type ReadableWritable = Types.ReadableWritable;
export const ReadableWritable = _ReadableWritable;
