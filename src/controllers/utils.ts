import type * as core from 'express-serve-static-core';
import {
  Request,
  Response,
  NextFunction,
  RequestHandler as ExpressRequestHandler,
} from 'express';
import type { JSONSchema7 } from 'json-schema';

import { TS } from '@avstantso/node-or-browser-js--utils';
import {
  validateBySchema,
  Model,
} from '@avstantso/node-or-browser-js--model-core';
import type * as Modeler from '@avstantso/node-js--modeler';

import { ApiError } from '@classes';
import MESSAGES from '@messages';

import type { Controller } from './types';

/**
 * @summary Create `RequestHandler` with call `next` for errors thrown by `requestHandler`
 */
export namespace Safe {
  export type TypeMap<
    Locals extends Record<string, any> = Record<string, any>
  > = {
    P?: core.ParamsDictionary;
    ResBody?: any;
    ReqBody?: any;
    ReqQuery?: core.Query;
    Locals?: Locals;
  };

  export type RequestHandler<
    R,
    T extends TypeMap,
    O extends TS.Override<TypeMap, T> = TS.Override<TypeMap, T>
  > = (
    req: Request<
      O['P'],
      O['ResBody'],
      O['ReqBody'],
      O['ReqQuery'],
      O['Locals']
    >,
    res: Response<O['ResBody'], O['Locals']>,
    next: NextFunction
  ) => R;

  export type Overload = <T extends TypeMap = TypeMap>(
    requestHandler: RequestHandler<unknown, T>
  ) => RequestHandler<Promise<unknown>, T>;
}

export type Safe<T extends Safe.TypeMap> = ExpressRequestHandler<
  T['P'] & {},
  T['ResBody'],
  T['ReqBody'],
  T['ReqQuery'] & {},
  T['Locals']
>;

export const Safe: Safe.Overload =
  (requestHandler) => async (req, res, next) => {
    try {
      return await requestHandler(req, res, next);
    } catch (e) {
      next(e);
    }
  };

export function validateData<TData = any>(
  data: TData,
  schema: JSONSchema7
): void {
  const { isValid, errors } = validateBySchema(data, schema);
  if (isValid) return;

  const e = ApiError.badRequest(MESSAGES.dataDoesNotMatchSchema);
  e.details = errors;
  throw e;
}

export function ModelerQuery<
  TModel extends Model.Structure,
  TModeler extends Modeler.Readable<TModel>
>(options: Controller.Options<TModel, TModeler>) {
  async function modelerQuery<T>(
    req: Request,
    res: Response,
    callback: (modeler: TModeler) => Promise<T>,
    isComplex?: boolean
  ) {
    const modeler = options.getModeler(req);

    const data = await callback(modeler);

    return res.json(
      isComplex
        ? {
            ...data,
          }
        : { data }
    );
  }

  modelerQuery.complex = async <T>(
    req: Request,
    res: Response,
    callback: (modeler: TModeler) => Promise<T>
  ) => modelerQuery<T>(req, res, callback, true);

  return modelerQuery;
}
