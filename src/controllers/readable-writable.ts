import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type * as Modeler from '@avstantso/node-js--modeler';

import type { Controller } from './types';

import Readable from './readable';
import Writable from './writable';

export default function <
  TModel extends Model.Structure,
  TModeler extends Modeler.ReadableWritable<TModel>
>(
  model: Model.Declaration<TModel>,
  options: Controller.ReadableWritable.Options<TModel, TModeler>
): Controller.ReadableWritable {
  const r = Readable<TModel, TModeler>(model, options);
  const w = Writable<TModel, TModeler>(model, options);
  return { ...r, ...w };
}
