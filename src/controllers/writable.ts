import { validate as isValidUUID } from 'uuid';

import type { Data, Model } from '@avstantso/node-or-browser-js--model-core';
import type * as Modeler from '@avstantso/node-js--modeler';

import { ApiError } from '../classes';
import MESSAGES from '../messages';

import type { Controller } from './types';
import { Safe, validateData } from './utils';

export default function <
  TModel extends Model.Structure,
  TModeler extends Modeler.Writable<TModel>
>(
  model: Model.Declaration<TModel>,
  options: Controller.Writable.Options<TModel, TModeler>
): Controller.Writable {
  //#region internal types aliases
  type Insert = TModel['Insert'];
  type Update = TModel['Update'];
  type Select = TModel['Select'];
  //#endregion

  const { getModeler, extender } = options;

  const insert = Safe(async (req, res, next) => {
    const { data } = req.body as Data<Insert>;

    if (!data) throw ApiError.badRequest(MESSAGES.dataNotSet);

    validateData(data, model.Schema.Insert);

    const modeler = getModeler(req);

    let entity: Select;
    if (extender?.insert)
      entity = await extender.insert({
        req,
        res,
        next,
        modeler,
        data,
      });
    else entity = await ApiError.catcher(modeler.insert(data));

    return res.json({
      data: entity,
    });
  });

  const update = Safe(async (req, res, next) => {
    const { data } = req.body as Data<Update>;

    if (!data) throw ApiError.badRequest(MESSAGES.dataNotSet);

    validateData(data, model.Schema.Update);

    const modeler = getModeler(req);

    let entity: Select;
    if (extender?.update)
      entity = await extender.update({
        req,
        res,
        next,
        modeler,
        data,
      });
    else entity = await ApiError.catcher(modeler.update(data));

    return res.json({
      data: entity,
    });
  });

  const _delete = Safe(async (req, res, next) => {
    const { id } = req.body as Model.IDed;

    if (!id) throw ApiError.badRequest(MESSAGES.idNotSet);

    if (!isValidUUID(id))
      throw ApiError.badRequest(`${MESSAGES.invalidUUIDFormatForId} "${id}"`);

    const modeler = getModeler(req);

    let data: Model.ID;
    if (extender?.delete)
      data = await extender.delete({
        req,
        res,
        next,
        modeler,
        id,
      });
    else data = await ApiError.catcher(modeler.delete(id));

    return res.json({
      data,
    });
  });

  return {
    insert,
    update,
    delete: _delete,
  };
}
