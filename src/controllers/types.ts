import type { Request, Response, RequestHandler, NextFunction } from 'express';

import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type * as Modeler from '@avstantso/node-js--modeler';

export namespace Controller {
  export type GetModeler<
    TModel extends Model.Structure,
    TModeler extends Modeler.Union.Any<TModel>
  > = (req: Request) => TModeler;

  export interface Options<
    TModel extends Model.Structure,
    TModeler extends Modeler.Union.Any<TModel>
  > {
    getModeler: GetModeler<TModel, TModeler>;
  }

  export namespace Readable {
    export interface Options<
      TModel extends Model.Structure,
      TModeler extends Modeler.Readable<TModel>
    > extends Controller.Options<TModel, TModeler> {}
  }

  export interface Readable {
    list: RequestHandler;
    one: RequestHandler;
  }

  export namespace Writable {
    export namespace Extender {
      export namespace Context {
        export interface Insert<
          TModel extends Model.Structure,
          TModeler extends Modeler.Writable<TModel>
        > extends Extender.Context<TModel, TModeler> {
          data: TModel['Insert'];
        }

        export interface Update<
          TModel extends Model.Structure,
          TModeler extends Modeler.Writable<TModel>
        > extends Extender.Context<TModel, TModeler> {
          data: TModel['Update'];
        }

        export interface Delete<
          TModel extends Model.Structure,
          TModeler extends Modeler.Writable<TModel>
        > extends Extender.Context<TModel, TModeler> {
          id: Model.ID;
        }
      }

      export interface Context<
        TModel extends Model.Structure,
        TModeler extends Modeler.Writable<TModel>
      > {
        req: Request;
        res: Response;
        next: NextFunction;
        modeler: TModeler;
      }

      export type Insert<
        TModel extends Model.Structure,
        TModeler extends Modeler.Writable<TModel>
      > = (
        context: Context.Insert<TModel, TModeler>
      ) => Promise<TModel['Select']>;

      export type Update<
        TModel extends Model.Structure,
        TModeler extends Modeler.Writable<TModel>
      > = (
        context: Context.Update<TModel, TModeler>
      ) => Promise<TModel['Select']>;

      export type Delete<
        TModel extends Model.Structure,
        TModeler extends Modeler.Writable<TModel>
      > = (context: Context.Delete<TModel, TModeler>) => Promise<Model.ID>;
    }

    export interface Extender<
      TModel extends Model.Structure,
      TModeler extends Modeler.Writable<TModel>
    > {
      insert?: Extender.Insert<TModel, TModeler>;
      update?: Extender.Update<TModel, TModeler>;
      delete?: Extender.Delete<TModel, TModeler>;
    }

    export interface Options<
      TModel extends Model.Structure,
      TModeler extends Modeler.Writable<TModel>
    > extends Controller.Options<TModel, TModeler> {
      extender?: Extender<TModel, TModeler>;
    }
  }

  export interface Writable {
    insert: RequestHandler;
    update: RequestHandler;
    delete: RequestHandler;
  }

  export namespace ReadableWritable {
    export interface Options<
      TModel extends Model.Structure,
      TModeler extends Modeler.ReadableWritable<TModel>
    > extends Readable.Options<TModel, TModeler>,
        Writable.Options<TModel, TModeler> {}
  }

  export type ReadableWritable = Readable & Writable;
}
