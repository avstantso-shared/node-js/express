import { validate as isValidUUID } from 'uuid';

import { Model } from '@avstantso/node-or-browser-js--model-core';
import type * as Modeler from '@avstantso/node-js--modeler';

import { ApiError } from '@classes';
import MESSAGES from '@messages';

import type { Controller } from './types';
import { Safe, ModelerQuery } from './utils';

export default function <
  TModel extends Model.Structure,
  TModeler extends Modeler.Readable<TModel>
>(
  model: Model.Declaration<TModel>,
  options: Controller.Readable.Options<TModel, TModeler>
): Controller.Readable {
  const modelerQuery = ModelerQuery<TModel, TModeler>(options);

  function deserializeSelectOptions(
    options: Model.Select.Options.Serialized
  ): Model.Select.Options<TModel> {
    const search =
      options.search &&
      JSON.parse(Buffer.from(options.search, 'base64').toString('utf8'));

    const pagination =
      options.pagination &&
      Model.Pagination(
        Number.parseInt(options.pagination.num, 10),
        Number.parseInt(options.pagination.size, 10)
      );

    return { search, pagination };
  }

  const list = Safe(async (req, res) =>
    modelerQuery.complex(req, res, ({ select }) => {
      const { search, pagination } = deserializeSelectOptions(req.query);

      if (pagination) {
        if (isNaN(pagination.num) || pagination.num < 0)
          throw ApiError.badRequest(
            MESSAGES.paginationNumMustBeEqualsOrGreaterThan0(pagination.num)
          );

        if (isNaN(pagination.size) || pagination.size <= 0)
          throw ApiError.badRequest(
            MESSAGES.paginationSizeMustBeGreaterThan0(pagination.size)
          );

        return select
          .pages(pagination, search)
          .then(([total, data]) => ({ total, data }));
      }

      return select(search).then((data) => ({ data }));
    })
  );

  const one = Safe(async (req, res) =>
    modelerQuery(req, res, ({ byId }) => {
      const { id } = req.params;

      if (!id) throw ApiError.badRequest(MESSAGES.idNotSet);

      if (!isValidUUID(id))
        throw ApiError.badRequest(`${MESSAGES.invalidUUIDFormatForId} "${id}"`);

      return byId(id);
    })
  );

  return {
    list,
    one,
  };
}
