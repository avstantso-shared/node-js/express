import type { Router, RequestHandler } from 'express';
import type * as Controller from '@controllers';

export interface RouterOverload<
  T extends Controller.Readable | Controller.Writable
> {
  (router: Router, controller: T, ...middlewares: RequestHandler[]): Router;
  (controller: T, ...middlewares: RequestHandler[]): Router;
}

export namespace RouterOverload {
  export type Readable = RouterOverload<Controller.Readable>;
  export type Writable = RouterOverload<Controller.Writable>;

  export namespace ReadableWritable {
    export interface Writable {
      Writable(...middlewares: RequestHandler[]): Router;
    }

    export interface Readable {
      Readable(...middlewares: RequestHandler[]): Writable;
    }
  }

  export interface ReadableWritable {
    (
      router: Router,
      controller: Controller.ReadableWritable,
      ...middlewares: RequestHandler[]
    ): ReadableWritable.Readable;
    (
      controller: Controller.ReadableWritable,
      ...middlewares: RequestHandler[]
    ): ReadableWritable.Readable;
  }
}
