import express, { Router, RequestHandler } from 'express';

import { JS } from '@avstantso/node-or-browser-js--utils';

import type * as Controller from '@controllers';

export function readParams<
  TController extends Controller.Readable | Controller.Writable
>(
  params: any[]
): {
  router: Router;
  controller: TController;
  middlewares: RequestHandler[];
} {
  if (JS.is.function(params[0])) {
    const [router, controller, ...middlewares] = params;
    return { router, controller, middlewares };
  }

  const [controller, ...middlewares] = params;
  return {
    router: express.Router({ mergeParams: true }),
    controller,
    middlewares,
  };
}
