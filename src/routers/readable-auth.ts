import type { RequestHandler } from 'express';

import type * as Controller from '@controllers';

import type { RouterOverload } from './types';
import Readable from './readable';
import { readParams } from './utils';

const RouterRAuth =
  (authMiddleware: RequestHandler): RouterOverload.Readable =>
  (...params: any[]) => {
    const { router, controller, middlewares } =
      readParams<Controller.Readable>(params);
    return Readable(router, controller, authMiddleware, ...middlewares);
  };

export default RouterRAuth;
