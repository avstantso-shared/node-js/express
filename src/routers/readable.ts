import express, { Router, RequestHandler } from 'express';

import type * as Controller from '@controllers';
import type { RouterOverload } from './types';
import { readParams } from './utils';

const Readable: RouterOverload.Readable = (...params: any[]): Router => {
  const { router, controller, middlewares } =
    readParams<Controller.Readable>(params);

  router.get('/', ...middlewares, controller.list);
  router.get('/:id', ...middlewares, controller.one);

  return router;
};

export default Readable;
