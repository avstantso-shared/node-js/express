import { RequestHandler } from 'express';

import type * as Controller from '@controllers';

import type { RouterOverload } from './types';
import Readable from './readable';
import Writable from './writable';
import { readParams } from './utils';

const ReadableWritable: RouterOverload.ReadableWritable = (
  ...params: any[]
) => {
  const { router, controller, middlewares } =
    readParams<Controller.ReadableWritable>(params);

  return {
    Readable: (...aMiddlewares: RequestHandler[]) => {
      Readable(router, controller, ...middlewares, ...aMiddlewares);
      return {
        Writable: (...aMiddlewares: RequestHandler[]) =>
          Writable(router, controller, ...middlewares, ...aMiddlewares),
      };
    },
  };
};

export default ReadableWritable;
