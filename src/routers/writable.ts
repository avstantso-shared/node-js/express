import { Router } from 'express';

import type * as Controller from '@controllers';

import type { RouterOverload } from './types';
import { readParams } from './utils';

const Writable: RouterOverload.Writable = (...params: any[]): Router => {
  const { router, controller, middlewares } =
    readParams<Controller.Writable>(params);

  router.put('/', ...middlewares, controller.insert);
  router.post('/', ...middlewares, controller.update);
  router.delete('/', ...middlewares, controller.delete);

  return router;
};

export default Writable;
