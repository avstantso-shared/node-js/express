import type { RequestHandler } from 'express';

import type * as Controller from '@controllers';

import type { RouterOverload } from './types';
import ReadableWritable from './readable-writable';
import { readParams } from './utils';

const RouterRWAuth =
  (authMiddleware: RequestHandler): RouterOverload.ReadableWritable =>
  (...params: any[]) => {
    const { router, controller, middlewares } =
      readParams<Controller.ReadableWritable>(params);
    return ReadableWritable(router, controller, authMiddleware, ...middlewares);
  };

export default RouterRWAuth;
