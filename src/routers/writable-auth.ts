import type { RequestHandler } from 'express';
import type * as Controller from '@controllers';

import type { RouterOverload } from './types';

import Writable from './writable';
import { readParams } from './utils';

const RouterWAuth =
  (authMiddleware: RequestHandler): RouterOverload.Writable =>
  (...params: any[]) => {
    const { router, controller, middlewares } =
      readParams<Controller.Writable>(params);
    return Writable(controller, router, authMiddleware, ...middlewares);
  };

export default RouterWAuth;
