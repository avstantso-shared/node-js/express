import type * as Express from 'express';
import jsonwebtoken from 'jsonwebtoken';

import { JS } from '@avstantso/node-or-browser-js--utils';

import type * as Types from './types';

export namespace Auth {
  export interface Options {
    readonly authName?: string;
    readonly message?: string;
    readonly jwtKey: jsonwebtoken.Secret;
  }

  export type Static<TAuthData> = (
    staticAuthData: TAuthData
  ) => Express.RequestHandler;

  export type RequestHandler<TAuthData> = Types.RequestHandler<TAuthData> & {
    static: Static<TAuthData>;
  };
}

const defaultAuthOptions: Auth.Options = {
  authName: 'auth',
  message: 'Not authorized',
  jwtKey: undefined,
};

export function Auth<TAuthData>(
  options?: Auth.Options
): Auth.RequestHandler<TAuthData> {
  const { authName, message, jwtKey } = { ...defaultAuthOptions, ...options };

  const auth: Auth.RequestHandler<TAuthData> = (req, res, next) => {
    if (req.method !== 'OPTIONS')
      try {
        // Bearer sdaklgharjkghsdfg
        const jwt = req.headers.authorization.split(' ')[1];
        if (!jwt) throw new Error();

        const decoded = jsonwebtoken.verify(jwt, jwtKey);

        JS.set.raw(req, authName, decoded);
      } catch (e) {
        return res.status(401).json({ message });
      }

    return next();
  };

  const staticAuth: Auth.Static<TAuthData> =
    (staticAuthData) => (req, res, next) => {
      if (req.method !== 'OPTIONS') JS.set.raw(req, authName, staticAuthData);

      return next();
    };

  auth.static = staticAuth;

  auth.FromReq = (req: Express.Request): TAuthData => JS.get.raw(req, authName);

  return auth;
}

Auth.defaultOptions = defaultAuthOptions;
