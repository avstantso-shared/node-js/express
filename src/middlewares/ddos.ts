import type { RequestHandler } from 'express';
import { JS } from '@avstantso/node-or-browser-js--utils';

const ACTION_DDOS_TIMEOUT =
  'test' !== process.env.NODE_ENV ? 5 * 60 * 1000 : 1000;
const ACTION_DDOS_CLEAR_LIMIT = 100;

// Registration from the same IP no more than once in ACTION_DDOS_TIMEOUT
// Clear every ACTION_DDOS_CLEAR_LIMIT registrations
export function Ddos(
  message: string,
  timeout = ACTION_DDOS_TIMEOUT,
  limit = ACTION_DDOS_CLEAR_LIMIT
): RequestHandler {
  const actionsDDOS = {};
  let actionsDDOSCounter = 0;

  const isInTimeoutDDOS = (cur: Date, ip: string): boolean => {
    const prevDate = actionsDDOS[ip as keyof object] as Date;
    return prevDate && prevDate.valueOf() > cur.valueOf() - timeout;
  };

  const DDOSClear = (cur: Date) => {
    actionsDDOSCounter++;
    if (actionsDDOSCounter <= limit) return;

    actionsDDOSCounter = 0;
    Object.keys(actionsDDOS).forEach((key) => {
      if (!isInTimeoutDDOS(cur, key)) delete actionsDDOS[key as keyof object];
    });
  };

  const retryAfter = (cur: Date, ip: string) =>
    (actionsDDOS[ip as keyof object] as Date).valueOf() +
    timeout -
    cur.valueOf();

  return (req, res, next) => {
    if (req.method !== 'OPTIONS') {
      const newDate = new Date();
      if (isInTimeoutDDOS(newDate, req.ip))
        return res
          .status(429)
          .setHeader('Retry-After', `${retryAfter(newDate, req.ip)}`)
          .json({ message });

      JS.set.raw(actionsDDOS, req.ip, newDate);
      DDOSClear(newDate);
    }

    return next();
  };
}

Ddos.defaultActionTimeout = ACTION_DDOS_TIMEOUT;
