import type * as Express from 'express';

export namespace FromReq {
  export interface Provider<TInReqData> {
    FromReq(req: Express.Request): TInReqData;
  }
}

export interface RequestHandler<TInReqData>
  extends Express.RequestHandler,
    FromReq.Provider<TInReqData> {}
